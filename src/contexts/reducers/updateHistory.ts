import {
  ADD,
  REMOVE,
  HistoryPayloadItem,
  HistoryPayloadActionTypes
} from "../conversionHistory/conversionHistoryContextTypes";

const updateHistoryReducer = (
  state: HistoryPayloadItem[] = [],
  action: HistoryPayloadActionTypes
) => {
  switch (action.type) {
    case ADD:
      return [action.payload, ...state];
    case REMOVE:
      return state.filter(
        element => !action.payload.value.includes(element.id)
      );
    default:
      return state;
  }
};

export default updateHistoryReducer;
