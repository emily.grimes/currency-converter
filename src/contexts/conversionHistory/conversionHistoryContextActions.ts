import {
  ADD,
  REMOVE,
  HistoryPayloadActionTypes,
  HistoryPayloadItem
} from "./conversionHistoryContextTypes";

export function add(item: HistoryPayloadItem): HistoryPayloadActionTypes {
  return {
    type: ADD,
    payload: item
  };
}

export function remove(items: Number[]): HistoryPayloadActionTypes {
  return { type: REMOVE, payload: { value: items } };
}
