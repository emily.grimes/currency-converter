import * as React from "react";
import updateHistoryReducer from "../reducers/updateHistory";
import {
  State,
  Dispatch,
  ProviderProps
} from "./conversionHistoryContextTypes";

// contexts
const ConvertedCurrenciesContext = React.createContext<State | undefined>(
  undefined
);
const ConvertedCurrenciesDispatchContext = React.createContext<
  Dispatch | undefined
>(undefined);

// Provider
export const ConvertedCurrenciesProvider = (props: ProviderProps) => {
  const [state, dispatch] = React.useReducer(updateHistoryReducer, []);
  return (
    <ConvertedCurrenciesContext.Provider value={state}>
      <ConvertedCurrenciesDispatchContext.Provider value={dispatch}>
        {props.children}
      </ConvertedCurrenciesDispatchContext.Provider>
    </ConvertedCurrenciesContext.Provider>
  );
};

// returns the state
export const useConvertedCurrenciesState = () => {
  const context = React.useContext(ConvertedCurrenciesContext);

  if (context === undefined) {
    throw new Error(
      "useConvertedCurrenciesState must be used within a ConvertedCurrenciesProvider"
    );
  }

  return context;
};

// returns a callable function that uses updateListReducer
export const useConvertedCurrenciesDispatch = () => {
  const context = React.useContext(ConvertedCurrenciesDispatchContext);

  if (context === undefined) {
    throw new Error(
      "useConvertedCurrenciesDispatch must be used within a ConvertedCurrenciesProvider"
    );
  }

  return context;
};
