// action types
export const ADD = "ADD";
export const REMOVE = "REMOVE";

// types
export type HistoryPayloadItem = {
  currencyFrom: string;
  valueFrom: number;
  currencyTo: string;
  valueTo: number;
  id: number;
};

export interface AddItemAction {
  type: typeof ADD;
  payload: HistoryPayloadItem;
}

export interface RemoveItemAction {
  type: typeof REMOVE;
  payload: {
    value: Number[];
  };
}

export type HistoryPayloadActionTypes = AddItemAction | RemoveItemAction;

export type Dispatch = (action: HistoryPayloadActionTypes) => void;
export type State = HistoryPayloadItem[];
export type ProviderProps = { children: React.ReactNode };
