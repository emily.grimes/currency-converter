// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import "jest-localstorage-mock";
import React from "react";

// this prevents console error printouts that obscure test results
// console will complain about useLayoutEffect on passing tests without this
React.useLayoutEffect = React.useEffect;
