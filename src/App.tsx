import React from "react";
import CustomButton from "./components/CustomButton";
import ArrowRightAltIcon from "@material-ui/icons/ArrowRightAlt";
import SettingsTwoToneIcon from "@material-ui/icons/SettingsTwoTone";
import RefreshIcon from "@material-ui/icons/Refresh";
import Table from "./components/Table";
import SettingsModal from "./components/SettingsModal";
import InputPlusSelect from "./components/InputPlusSelect";
import BackspaceIcon from "@material-ui/icons/Backspace";
import Tooltip from "@material-ui/core/Tooltip";
import { useConvertedCurrenciesDispatch } from "./contexts/conversionHistory/conversionHistoryContext";
import { add } from "./contexts/conversionHistory/conversionHistoryContextActions";
import { apiURL } from "./constants/urls";
import { Rates } from "./constants/interfaces";
import { ChangeEvent } from "./constants/types";
import { formatNumber } from "./helpers/formatNumber";
import {
  checkLocalStorage,
  getJSONFromLocalStorage,
  setJSONToLocalStorage
} from "./helpers/setAndGetFromLocalStorage";

const App: React.FC = () => {
  const [currencyFrom, setCurrencyFrom] = React.useState("USD");
  const [currencyTo, setCurrencyTo] = React.useState("USD");
  const [valueFrom, setValueFrom] = React.useState(0.0);
  const [valueTo, setValueTo] = React.useState(0.0);
  const [conversionRates, setConversionRates] = React.useState<Rates>({});
  const [viewModal, setViewModal] = React.useState(false);
  const [historyCount, setHistoryCount] = React.useState(0);
  const [originalCurrencyList, setOriginalCurrencyList] = React.useState([""]);

  const dispatch = useConvertedCurrenciesDispatch();

  const getRatesFromApi = async () => {
    const list = await fetch(`${apiURL}${currencyFrom}`);
    const { rates } = await list.json();
    return rates;
  };

  const refreshLocalStorage = async () => {
    setJSONToLocalStorage(currencyFrom, await getRatesFromApi());
  };

  const getRates = async () => {
    if (!checkLocalStorage(currencyFrom)) {
      const rates = await getRatesFromApi();
      setConversionRates(rates);
      setJSONToLocalStorage(currencyFrom, rates);
    } else {
      setRatesFromLocalStorage();
    }
  };

  const setRatesFromLocalStorage = () => {
    const savedRates = getJSONFromLocalStorage(currencyFrom);
    if (savedRates !== null) {
      setConversionRates(savedRates);
    }
  };

  const handleCurrencyFromChoice = (event: ChangeEvent) => {
    setCurrencyFrom(event.target.value);
  };

  const handleCurrencyToChoice = (event: ChangeEvent) => {
    setCurrencyTo(event.target.value);
  };

  const handleValueFromChoice = (event: ChangeEvent) => {
    setValueFrom(
      event.target.value !== "" ? Number.parseFloat(event.target.value) : null
    );
  };

  const handleClear = () => {
    setCurrencyFrom("USD");
    setCurrencyTo("USD");
    setValueFrom(0);
  };

  const handleAddToHistory = () => {
    dispatch(
      add({
        currencyFrom,
        valueFrom,
        currencyTo,
        valueTo,
        id: historyCount
      })
    );
    setHistoryCount(historyCount + 1);
  };

  React.useEffect(() => {
    const launch = async () => {
      const rates = await getRatesFromApi();
      setConversionRates(rates);
      setOriginalCurrencyList(Object.keys(rates));
    };

    launch();
  }, []);

  React.useEffect(() => {
    getRates();
  }, [currencyFrom, viewModal]);

  React.useEffect(() => {
    setValueTo(formatNumber(valueFrom * conversionRates[currencyTo], 2));
  }, [valueFrom, currencyTo, conversionRates]);

  const message = `${valueFrom === null ? "0" : valueFrom} ${currencyFrom} = ${
    valueTo === null ? "0" : valueTo
  } ${currencyTo}`;

  const conversionDataExists = typeof conversionRates[currencyTo] === "number";

  const currencyList = Object.keys(conversionRates);

  return (
    <div className="app">
      <h1>Currency Conversion</h1>

      <div className="inputs">
        <InputPlusSelect
          value={valueFrom === null ? "" : valueFrom}
          selection={currencyFrom}
          handleInputChange={handleValueFromChoice}
          handleSelectChange={handleCurrencyFromChoice}
          currencyList={originalCurrencyList}
        />
        <ArrowRightAltIcon />
        <InputPlusSelect
          value={
            currencyTo !== "" && conversionDataExists
              ? valueFrom * conversionRates[currencyTo]
              : ""
          }
          selection={currencyTo}
          handleInputChange={() => {}}
          handleSelectChange={handleCurrencyToChoice}
          currencyList={currencyList}
        />
        <Tooltip title="Rate Settings" aria-label="Edit Exchange Rates">
          <SettingsTwoToneIcon onClick={() => setViewModal(!viewModal)} />
        </Tooltip>
        <Tooltip title="Refresh Rates" aria-label="Refresh rates from API">
          <RefreshIcon
            onClick={async () => {
              setConversionRates(await getRatesFromApi());
              refreshLocalStorage();
            }}
          />
        </Tooltip>
      </div>
      {viewModal && (
        <SettingsModal
          data={Object.keys(conversionRates).map(e => {
            return { name: e, rate: conversionRates[e] };
          })}
          currencyFrom={currencyFrom}
          setViewModal={setViewModal}
        />
      )}
      {conversionRates && (
        <p>
          {message} <span />
          <Tooltip title="Clear Inputs" aria-label="Clear the form">
            <BackspaceIcon onClick={handleClear} />
          </Tooltip>
        </p>
      )}
      <CustomButton
        onClick={handleAddToHistory}
        variant="contained"
        color="primary"
      >
        Add to history
      </CustomButton>

      <Table />
    </div>
  );
};

export default App;
