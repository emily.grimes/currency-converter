const getStringFromLocalStorage = (key: string): string => {
  return localStorage.getItem(key);
};

export const getJSONFromLocalStorage = (key: string) => {
  return JSON.parse(getStringFromLocalStorage(key));
};

export const setJSONToLocalStorage = (key: string, value: Object): void => {
  localStorage.setItem(key, JSON.stringify(value));
};

export const checkLocalStorage = (key: string): boolean => {
  return getStringFromLocalStorage(key) !== null;
};
