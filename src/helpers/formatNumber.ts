export const parseFormatedNumber = (
  str: string,
  decimalPlace: number
): number => {
  return formatNumber(Number.parseFloat(str), decimalPlace);
};

export const formatNumber = (num: number, decimalPlace: number): number => {
  return Number.parseFloat(num.toFixed(decimalPlace));
};
