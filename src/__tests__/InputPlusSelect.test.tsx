import React from "react";
import InputPlusSelect from "../components/InputPlusSelect";
import renderer from "react-test-renderer";

const testProps = {
  valueString: "",
  valueNumber: 1,
  selection: "",
  currencyList: [""]
};

describe("InputPlusSelect", () => {
  test("renders correctly with a number value", () => {
    const tree = renderer
      .create(
        <InputPlusSelect
          value={testProps.valueNumber}
          selection={testProps.selection}
          currencyList={testProps.currencyList}
          handleInputChange={() => {}}
          handleSelectChange={() => {}}
        />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  test("renders correctly with a string value", () => {
    const tree = renderer
      .create(
        <InputPlusSelect
          value={testProps.valueString}
          selection={testProps.selection}
          currencyList={testProps.currencyList}
          handleInputChange={() => {}}
          handleSelectChange={() => {}}
        />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
