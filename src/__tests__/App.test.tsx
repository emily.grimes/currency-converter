import React from "react";
import App from "../App";
import renderer from "react-test-renderer";
import { ConvertedCurrenciesProvider } from "../contexts/conversionHistory/conversionHistoryContext";

test("App renders correctly", () => {
  const tree = renderer
    .create(
      <ConvertedCurrenciesProvider>
        <App />
      </ConvertedCurrenciesProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
