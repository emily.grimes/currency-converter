import { parseFormatedNumber, formatNumber } from "../../helpers/formatNumber";

describe("parseFormatNumber", () => {
  test("takes a string returns a number", () => {
    const string2 = "2";
    const number2 = 2;
    const string202 = "202.123";
    const number202 = 202.12;
    expect(parseFormatedNumber(string2, 0)).toBe(number2);
    expect(parseFormatedNumber(string202, number2)).toBe(number202);
  });
});

describe("formatNumber", () => {
  test("formats a number to a certain number of decimal places", () => {
    const number = 12.12345;
    const places = 1;
    const places2 = 2;
    expect(formatNumber(number, places)).toBe(12.1);
    expect(formatNumber(number, places2)).toBe(12.12);
  });
});
