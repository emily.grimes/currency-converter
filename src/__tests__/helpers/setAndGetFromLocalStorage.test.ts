import {
  getJSONFromLocalStorage,
  setJSONToLocalStorage,
  checkLocalStorage
} from "../../helpers/setAndGetFromLocalStorage";

const key = "testing";
const value = { something: 12 };
const valueString = JSON.stringify(value);

describe("setAndGetFromLocalStorageHelers", () => {
  beforeEach(() => {
    // values stored in tests will also be available in other tests unless you run
    localStorage.clear();
  });
  describe("getJSONFromLocalStorage", () => {
    test("getItem is called", () => {
      getJSONFromLocalStorage(key);
      expect(localStorage.getItem).toHaveBeenCalledWith(key);
    });
  });

  describe("setJSONToLocalStorage", () => {
    test("setItem is called", () => {
      setJSONToLocalStorage(key, value);
      expect(localStorage.setItem).toHaveBeenCalledWith(key, valueString);
    });
  });

  describe("checkLocalStorage", () => {
    test("getItem is called", () => {
      checkLocalStorage(key);
      expect(localStorage.getItem).toHaveBeenCalledWith(key);
    });
  });
});
