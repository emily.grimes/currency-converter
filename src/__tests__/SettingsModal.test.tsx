import React from "react";
import SettingsModal from "../components/SettingsModal";
import renderer from "react-test-renderer";

// SettingsModal renders a materialui component that uses refs which break
// in react-test-renderer. Jest creators recommend mocking third party
// components to prevent this breaking error.
jest.mock("../components/EditableTable", () => "EditableTable");

const testProps = {
  data: [{ name: "string", rate: 0 }],
  currencyFrom: "STR",
  setViewModal: (variable: boolean) => console.log(variable)
};

test("SettingsModal renders correctly", () => {
  const tree = renderer
    .create(
      <SettingsModal
        data={testProps.data}
        currencyFrom={testProps.currencyFrom}
        setViewModal={testProps.setViewModal}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
