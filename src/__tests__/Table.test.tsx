import React from "react";
import Table from "../components/Table";
import renderer from "react-test-renderer";
import { ConvertedCurrenciesProvider } from "../contexts/conversionHistory/conversionHistoryContext";

test("Table renders correctly", () => {
  const tree = renderer
    .create(
      <ConvertedCurrenciesProvider>
        <Table />
      </ConvertedCurrenciesProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
