import React from "react";
import EditableTable from "../components/EditableTable";
import renderer from "react-test-renderer";

// EditableTable renders a materialui component that uses refs which break
// in react-test-renderer. Jest creators recommend mocking third party
// components to prevent this breaking error.
jest.mock("../components/EditableTable", () => "EditableTable");

const testProps = {
  data: [{ name: "string", rate: 0 }],
  currencyFrom: "STR",
  setData: () => [{ name: "string", rate: 1 }]
};

test("EditableTable renders correctly", () => {
  const tree = renderer
    .create(
      <EditableTable
        data={testProps.data}
        currencyFrom={testProps.currencyFrom}
        setData={testProps.setData}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
