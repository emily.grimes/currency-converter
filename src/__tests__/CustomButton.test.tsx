import React from "react";
import CustomButton from "../components/CustomButton";
import renderer from "react-test-renderer";

test("CustomButton renders correctly", () => {
  const tree = renderer.create(<CustomButton>Some text</CustomButton>).toJSON();
  expect(tree).toMatchSnapshot();
});
