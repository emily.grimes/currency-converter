import React from "react";
import BootstrapInput from "../components/BootstrapInput";
import renderer from "react-test-renderer";

test("BootstrapInput renders correctly", () => {
  const tree = renderer.create(<BootstrapInput />).toJSON();
  expect(tree).toMatchSnapshot();
});
