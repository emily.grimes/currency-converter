import React, { useState } from "react";
import EditableTable from "./EditableTable";
import CustomButton from "../components/CustomButton";
import { Rates, Rate } from "../constants/interfaces";
import { setJSONToLocalStorage } from "../helpers/setAndGetFromLocalStorage";
import styles from "./SettingsModal.module.css";

interface SettingsModalProps {
  data: Rate[];
  currencyFrom: string;
  setViewModal: (viewModal: boolean) => void;
}

const convertArrayToObject = (array: Rate[]): Rates => {
  const rates = {};
  return array.reduce((obj: Rates, item: Rate) => {
    return {
      ...obj,
      [item["name"]]: item["rate"]
    };
  }, rates);
};

const SettingsModal = (props: SettingsModalProps) => {
  const [settings, setSettings] = useState(props.data);

  const updateData = (callback: () => Rate[]) => {
    const newRatesArr = callback();
    setSettings(newRatesArr);
    updateLocalStorage();
  };

  const updateLocalStorage = () => {
    setJSONToLocalStorage(props.currencyFrom, convertArrayToObject(settings));
  };

  return (
    <div className={styles.modal}>
      <div className={styles.modalContent}>
        <EditableTable
          data={settings}
          currencyFrom={props.currencyFrom}
          setData={updateData}
        />
        <CustomButton
          variant="contained"
          onClick={() => props.setViewModal(false)}
        >
          Cancel
        </CustomButton>
        <CustomButton
          variant="contained"
          color="secondary"
          onClick={() => {
            updateLocalStorage();
            props.setViewModal(false);
          }}
        >
          Update Rates
        </CustomButton>
      </div>
    </div>
  );
};

export default SettingsModal;
