import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import NativeSelect from "@material-ui/core/NativeSelect";
import BootstrapInput from "./BootstrapInput";
import { apiURL } from "../constants/urls";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    margin: {
      margin: theme.spacing(1)
    }
  })
);

interface InputPlusSelectProps {
  value: number | string;
  selection: string;
  currencyList: string[];
  handleInputChange: (event: React.ChangeEvent<{ value: string }>) => void;
  handleSelectChange: (event: React.ChangeEvent<{ value: string }>) => void;
}

const InputPlusSelect: React.FC<InputPlusSelectProps> = (
  props: InputPlusSelectProps
) => {
  const classes = useStyles();

  return (
    <div className="oneInput">
      <FormControl className={classes.margin}>
        <InputLabel htmlFor="demo-customized-textbox">Amount</InputLabel>
        <BootstrapInput
          inputProps={{ type: "number", step: ".01", placeholder: "0.00" }}
          value={props.value}
          onChange={props.handleInputChange}
          id="demo-customized-textbox"
        />
      </FormControl>

      <FormControl className={classes.margin}>
        <InputLabel
          style={{ display: "none" }}
          htmlFor="demo-customized-select-native"
        >
          Currency Type
        </InputLabel>
        <NativeSelect
          id="demo-customized-select-native"
          value={props.selection}
          onChange={props.handleSelectChange}
          input={<BootstrapInput />}
        >
          <option value="" />
          {props.currencyList &&
            props.currencyList.map((item, index) => (
              <option key={`$item-${index}`} value={item}>
                {item}
              </option>
            ))}
        </NativeSelect>
      </FormControl>
    </div>
  );
};

export default InputPlusSelect;
