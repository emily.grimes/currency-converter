import { createStyles, withStyles, Theme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const CustomButton = withStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: `${theme.spacing(1.5)}px 0`,
      width: "100%",
      alignSelf: "center"
    }
  })
)(Button);

export default CustomButton;
