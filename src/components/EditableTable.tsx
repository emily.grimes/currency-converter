import React from "react";
import MaterialTable from "material-table";
import { parseFormatedNumber } from "../helpers/formatNumber";
import { Rate } from "../constants/interfaces";

interface TableProps {
  data: Rate[];
  currencyFrom: string;
  setData: (callback: () => Rate[]) => void;
}

const EditableTable = (props: TableProps) => {
  const columns = [
    {
      title: "Name",
      field: "name"
    },
    {
      title: "Rate",
      field: "rate"
    }
  ];

  return (
    <MaterialTable
      title="Conversion Rates"
      columns={columns}
      data={props.data}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              newData.rate =
                typeof newData.rate === "string"
                  ? parseFormatedNumber(newData.rate, 2)
                  : newData.rate;
              props.setData(() => {
                return [...props.data, newData];
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                newData.rate =
                  typeof newData.rate === "string"
                    ? parseFormatedNumber(newData.rate, 2)
                    : newData.rate;
                props.setData(() => {
                  const data = [...props.data];
                  data[data.indexOf(oldData)] = newData;
                  return data;
                });
              }
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              props.setData(() => {
                const data = [...props.data];
                data.splice(data.indexOf(oldData), 1);
                return data;
              });
            }, 600);
          })
      }}
    />
  );
};
export default EditableTable;
