import React from "react";
import ReactDOM from "react-dom";
import { ConvertedCurrenciesProvider } from "./contexts/conversionHistory/conversionHistoryContext";
import "./index.css";
import App from "./App";

const rootElement = document.getElementById("root");

ReactDOM.render(
  <ConvertedCurrenciesProvider>
    <App />
  </ConvertedCurrenciesProvider>,
  rootElement
);
