// typescript types

export type ChangeEvent = React.ChangeEvent<{ value: string }>;
