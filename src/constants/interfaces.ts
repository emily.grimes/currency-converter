export interface Rates {
  [key: string]: number;
}

export interface Rate {
  name: string;
  rate: number;
}
